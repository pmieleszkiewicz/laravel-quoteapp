<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/materialize.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


</head>
<body>

    <!-- Account dropdown -->
    <ul id="account_dropdown" class="dropdown-content">
        <li><a href="#!">My quotes</a></li>
        <li class="divider"></li>
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>

    <!-- Account dropdown for mobile nav - cannot use one dropdown source in Materialize -->
    <ul id="account_dropdown_mobile" class="dropdown-content">
        <li><a href="#!">My quotes</a></li>
        <li class="divider"></li>
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>

    <nav style="margin-bottom: 50px;">
        <div class="nav-wrapper blue">
            <a href="{{ route('quote.index') }}" class="brand-logo center">QuoteApp</a>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="{{ route('quote.index') }}">Home</a></li>
                @guest
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li><a href="{{ route('quote.create') }}">Add quote</a></li>
                    <!-- Dropdown Trigger -->
                    <li><a class="dropdown-button" href="#" data-activates="account_dropdown">{{ Auth::user()->name }}<i class="material-icons right">arrow_drop_down</i></a></li>
                @endguest
            </ul>
            <ul class="side-nav white" id="mobile-demo">
                <li><a href="{{ route('quote.index') }}" class="blue-text"><i class="material-icons blue-text">home</i>Home</a></li>
                @auth
                    <li><a href="{{ route('quote.create') }}" class="blue-text"><i class="material-icons blue-text">add_circle</i> Add quote</a></li>
                    <li><a class="dropdown-button" href="#" data-activates="account_dropdown_mobile"> <span class="blue-text">{{ Auth::user()->name }}</span><i class="material-icons right blue-text">arrow_drop_down</i></a></li>
                @else
                    <li><a href="{{ route('login') }}" class="blue-text"><i class="material-icons blue-text">account_box</i>Login</a></li>
                    <li><a href="{{ route('register') }}" class="blue-text"><i class="material-icons blue-text">supervisor_account</i>Register</a></li>
                @endauth
            </ul>
        </div>
    </nav>

    <div class="container">
        @yield('content')
    </div>



    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/materialize.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
