@extends('layouts/app')

@section('content')

    <div class="row">

        <form class="col s8 offset-s2" action="{{ route('quote.store') }}" method="POST">

            @if ($errors->any())
                <ul class="collection red-text">
                    @foreach ($errors->all() as $error)
                        <li class="collection-item"><i class="small material-icons red-text">error_outline</i> {{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <div class="row">
                <div class="input-field col s12">
                    <textarea id="quote_content" name="quote" type="text" class="materialize-textarea validate" required></textarea>
                    <label for="quote_content" data-error="This field is required!" data-success="">Quote</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <input id="quote_author" name="author" type="text" class="validate" required>
                    <label for="quote_author" data-error="This field is required!" data-success="">Author</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <select name="color">
                        <option value="" disabled selected>Select Your color</option>
                        <option value="green">Green</option>
                        <option value="orange">Orange</option>
                        <option value="pink">Pink</option>
                    </select>
                    <label>Select Your color</label>
                </div>
            </div>


            <div class="row">
                <button class="waves-effect waves-light btn blue darken-2" role="button" type="submit">Create</button>
            </div>
            {{ csrf_field() }}

        </form>

    </div>

@endsection