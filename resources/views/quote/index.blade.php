@extends('layouts/app')

@section('content')




    <div class="row">
        @foreach ($quotes as $quote)

            <div class="col s12 l6">
                <div class="card {{ $quote->color }} lighten-2">
                    <div class="card-content white-text">
                        <p>{{ $quote->quote }}</p>
                        <p class="right-align">~ <i>{{ $quote->author }}</i></p>
                    </div>
                </div>
            </div>
        @endforeach

    </div>

@endsection